# Obsidian Documentation #


### Structure ###

* /doc/ : various documentation
* /obsidian/api/Obsidian/Integration : code samples for Obsidian Integration tools
* /obsidian/api/Obsidian/CMDB : code samples for Obsidian CMDB tools