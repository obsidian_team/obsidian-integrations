<?php 
error_reporting(E_ALL ^ E_NOTICE);
include_once("header.php");

// PRUEBA 1 - A PARTIR DE LOS DATOS EN BSM_SERVICE_VALUE 
// php calculate_hist.php --service=2 --term --verbose=2 --start=2017-07-01 --end=2017-07-31 --redohist --immediatecalc  --nodaemon

// PRUEBA 2 - A PARTIR DE LOS DATOS MENSUALES EN STATS
//  php calculate_hist.php --service=2 --term --verbose=2 --start=2017-07-01 --end=2017-07-31 --redohist --immediatecalc --onlymonth --nodaemon


$svId=1;

// Add last values
//$svId = $linkObsidian->last_id();

$linkObsidian->query("truncate bsm_service_value");
$linkObsidian->query("truncate bsm_service_value_stats"); 
$linkObsidian->query("truncate bsm_service_data2text;");


$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",1,100,'".date('2017-01-01')."','".date('2017-01-01')."',0)");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",2,100,'".date('2017-01-01')."','".date('2017-01-01')."',0)");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",3,100,'".date('2017-01-01')."','".date('2017-01-01')."',0)");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",4,100,'".date('2017-01-01')."','".date('2017-01-01')."',0)");


$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored,rootcause) values (".$svId.",1,91,'".date('2017-07-10')."','".date('2017-07-10')."',0,'27253')");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored,rootcause) values (".$svId.",4,91,'".date('2017-07-10')."','".date('2017-07-10')."',0,'27253')");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",1,100,'".date('2017-07-13')."','".date('Y-M-D')."',0)");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",4,100,'".date('2017-07-13')."','".('2017-07-18')."',0)");


$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored,rootcause) values (".$svId.",3,85,'".date('2017-07-18')."','".date('2017-07-18')."',0,'26753')");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored,rootcause) values (".$svId.",4,85,'".date('2017-07-18')."','".date('2017-07-18')."',0,'26753')");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",3,100,'".date('2017-07-19')."','".date('Y-M-D')."',0)");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",4,100,'".date('2017-07-19')."','".date('2017-07-26')."',0)");
   
 
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored,rootcause) values (".$svId.",1,91,'".date('2017-07-22')."','".date('2017-07-22')."',0,'27538,27539')");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored,rootcause) values (".$svId.",4,91,'".date('2017-07-22')."','".date('2017-07-22')."',0,'27538,27539')");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",1,100,'".date('2017-07-26')."','".date('Y-M-D')."',0)");
$linkObsidian->query("insert into bsm_service_value(id_bsm_service, id_bsm_variable, value, last_change, last_update,isIgnored) values (".$svId.",4,100,'".date('2017-07-26')."','".date('Y-M-D')."',0)");
    
// Add historic monthly values
/*  
for ($j=1; $j <= 12; $j++) {
    $defaultValue=100;
    $prevMonth=date('Y-m-d', strtotime(date('Y-m-01')." -".$j." month"));
    $linkObsidian->query("Insert into bsm_service_value_stats(id_bsm_service, id_bsm_variable, value, last_update,period,date,min,max,deviation,first_quartile,third_quartile) values (".$svId.",1,$defaultValue,'".date('Y-m-d H:i:s')."','2','".$prevMonth."',$defaultValue,$defaultValue,0,$defaultValue,$defaultValue)");
    $linkObsidian->query("Insert into bsm_service_value_stats(id_bsm_service, id_bsm_variable, value, last_update,period,date,min,max,deviation,first_quartile,third_quartile) values (".$svId.",2,$defaultValue,'".date('Y-m-d H:i:s')."','2','".$prevMonth."',$defaultValue,$defaultValue,0,$defaultValue,$defaultValue)");
    $linkObsidian->query("Insert into bsm_service_value_stats(id_bsm_service, id_bsm_variable, value, last_update,period,date,min,max,deviation,first_quartile,third_quartile) values (".$svId.",3,$defaultValue,'".date('Y-m-d H:i:s')."','2','".$prevMonth."',$defaultValue,$defaultValue,0,$defaultValue,$defaultValue)");
    $linkObsidian->query("Insert into bsm_service_value_stats(id_bsm_service, id_bsm_variable, value, last_update,period,date,min,max,deviation,first_quartile,third_quartile) values (".$svId.",4,$defaultValue,'".date('Y-m-d H:i:s')."','2','".$prevMonth."',$defaultValue,$defaultValue,0,$defaultValue,$defaultValue)");
}
 */

// Add historic daily values
/*
for ($j=1; $j <= 31; $j++) {
    $defaultValue=100;
    $prevMonth=date("Y-07-$j");
    
 if (($j==10) or ($j==11) or ($j==12) or ($j==20)) $defaultValue=0; else $defaultValue=100;
    
    $linkObsidian->query("Insert into bsm_service_value_stats(id_bsm_service, id_bsm_variable, value, last_update,period,date,min,max,deviation,first_quartile,third_quartile) values (".$svId.",1,$defaultValue,'".date('Y-m-d H:i:s')."','1','".$prevMonth."',$defaultValue,$defaultValue,0,$defaultValue,$defaultValue)");
    $linkObsidian->query("Insert into bsm_service_value_stats(id_bsm_service, id_bsm_variable, value, last_update,period,date,min,max,deviation,first_quartile,third_quartile) values (".$svId.",2,$defaultValue,'".date('Y-m-d H:i:s')."','1','".$prevMonth."',$defaultValue,$defaultValue,0,$defaultValue,$defaultValue)");
    $linkObsidian->query("Insert into bsm_service_value_stats(id_bsm_service, id_bsm_variable, value, last_update,period,date,min,max,deviation,first_quartile,third_quartile) values (".$svId.",3,$defaultValue,'".date('Y-m-d H:i:s')."','1','".$prevMonth."',$defaultValue,$defaultValue,0,$defaultValue,$defaultValue)");
    $linkObsidian->query("Insert into bsm_service_value_stats(id_bsm_service, id_bsm_variable, value, last_update,period,date,min,max,deviation,first_quartile,third_quartile) values (".$svId.",4,$defaultValue,'".date('Y-m-d H:i:s')."','1','".$prevMonth."',$defaultValue,$defaultValue,0,$defaultValue,$defaultValue)");
}
*/
  

echo ("OK");