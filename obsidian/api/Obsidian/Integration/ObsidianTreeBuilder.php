<?php

namespace Obsidian\Integration;

use Obsidian\Tools\Database;

abstract class ObsidianTreeBuilder implements TreeBuilder
{
    use TreeBuilderHeaders;

    /**
     * @var array
     */
    protected $config = array();

    /** todo: remove this
     * @var \mysqli|null
     */
    protected $linkObsidian = null;

    /**
     * @var Database
     */
    protected $dbService;

    /**
     * @var string
     */
    protected $error = '';

    /**
     * @var bool
     */
    protected $available = true;

    /**
     * @var int
     */
    protected $rootId = 0;

    /**
     * @var array|mixed
     */
    protected $toolParams = array();

    function __construct($config)
    {
        $this->config = $config;
        // todo: remove this, pass dbService
        if (isset($config['mysql_host'])) {
            $this->linkObsidian = mysqli_connect($config['mysql_host'], $config['mysql_user'], $config['mysql_pasw']);
            mysqli_select_db($this->linkObsidian, $config['mysql_db']); // TODO check the return type
        }
        // todo: pass request object, or using setter
        if (isset($_REQUEST['id'])) {
            $this->rootId = $_REQUEST['id'];
        }
        $tmp = $this->getSourceTools();
        $this->toolParams = $tmp[$this->getToolKeyname()];
        $this->initializeResources();
    }

    function __destruct()
    {
        $this->close();
        $this->freeResources();
    }

    /**
     * @param Database $db
     */
    public function setDbService(Database $db)
    {
        $this->dbService = $db;
    }

    public function startsWith($haystack, $needle)
    {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }

    public function ciInUse($ci, $id_bsm_customer)
    {
        $inUse = "";
        if ($this->linkObsidian) {
            $sql2 = "select b.name from bsm_service_tree a, bsm_service b where a.id_bsm_service=b.id and b.id_bsm_customer=$id_bsm_customer and a.ci_monitor='$ci' and !(ci_type='monitor' or ci_type='availability' or ci_type='capacity' or ci_type='synthetictransaction' or ci_type='kpi' or ci_type='servicedesk')";
            $res2 = mysqli_query($this->linkObsidian, $sql2);
            if (mysqli_num_rows($res2) > 1) {
                $tooltip = '';
                $comma = '';
                while ($row = mysqli_fetch_array($res2)) {
                    $tooltip .= $comma.$row['name'];
                    $comma = ', ';
                }
                $inUse = "aCol='red' sCol='red' tooltip='".$tooltip."'";
            } else {
                if (mysqli_num_rows($res2) > 0) {
                    $row = mysqli_fetch_array($res2);
                    $inUse = "aCol='#cccccc' sCol='#cccccc' tooltip='".htmlspecialchars($row['name'], ENT_QUOTES)."'";
                }
            }
        }

        return $inUse;
    }

    // AÃ±adir la comprobaciÃ³n de que es un servicio
    // todo: used only in Azure, replace with ciInUse?
    public function monitorInUse($monitor, $id_bsm_customer)
    {
        $inUse = "";
        if ($this->linkObsidian) {
            $sql2 = "select b.name from bsm_service_tree a, bsm_service b where a.id_bsm_service=b.id and b.id_bsm_customer=$id_bsm_customer and a.ci_monitor='$monitor' and (ci_type='monitor' or ci_type='availability' or ci_type='capacity' or ci_type='synthetictransaction' or ci_type='kpi' or ci_type='servicedesk')";
            $res2 = mysqli_query($this->linkObsidian, $sql2);
            if (mysqli_num_rows($res2) > 1) {
                $tooltip = '';
                $comma = '';
                while ($row = mysqli_fetch_array($res2)) {
                    $tooltip .= $comma.$row['name'];
                    $comma = ', ';
                }
                $inUse = "aCol='red' sCol='red' tooltip='".$tooltip."'";
            } else {
                if (mysqli_num_rows($res2) > 0) {
                    $row = mysqli_fetch_array($res2);
                    $inUse = "aCol='#cccccc' sCol='#cccccc' tooltip='".$row['name']."'";
                }
            }
        }

        return $inUse;
    }

    // AÃ±adir la comprobaciÃ³n de que es un hostcheck
    public function hostcheckInUse($monitor, $id_bsm_customer)
    {
        $inUse = "";
        if ($this->linkObsidian) {
            $sql2 = "select b.name from bsm_service_tree a, bsm_service b where a.id_bsm_service=b.id and b.id_bsm_customer=$id_bsm_customer and a.ci_monitor='$monitor' and (ci_type='monitor' or ci_type='availability' or ci_type='capacity' or ci_type='synthetictransaction' or ci_type='kpi' or ci_type='servicedesk')";
            $res2 = mysqli_query($this->linkObsidian, $sql2);
            if (mysqli_num_rows($res2) > 1) {
                $tooltip = '';
                $comma = '';
                while ($row = mysqli_fetch_array($res2)) {
                    $tooltip .= $comma.$row['name'];
                    $comma = ', ';
                }
                $inUse = "aCol='red' sCol='red' tooltip='".$tooltip."'";
            } else {
                if (mysqli_num_rows($res2) > 0) {
                    $row = mysqli_fetch_array($res2);
                    $inUse = "aCol='#cccccc' sCol='#cccccc' tooltip='".$row['name']."'";
                }
            }
        }

        return $inUse;
    }

    public function hasError()
    {
        return isset($this->error) && $this->error;
    }

    public function logError()
    {
        error_log($this->error);
    }

    public function outputError()
    {
        if ($this->error) {
            print $this->error."\n";
        } else {
            $this->sendHeaders();
            print '<tree id="0"><item id="error" text="Error"></item></tree>';
        }
    }

    public function outputTree()
    {
        $this->sendHeaders();
        // todo: user request object inject instead of direct $_REQUEST and $_GET
        $fname = dirname(dirname(dirname(dirname(__FILE__)))).'/console/uploaded/tool_'.$this->className().'_'.urlencode($this->rootId).'_'.urlencode($_GET['filter']).'_'.urlencode($_GET['filter2']).'.xml';
        if (file_exists($fname) && isset($this->config['toolXmlReloadPeriod']) && (date('Y-m-d H:i:s',
                    filemtime($fname)) > date('Y-m-d H:i:s',
                    strtotime(date('Y-m-d H:i:s')." -".$this->config['toolXmlReloadPeriod']." minute"))) && $_REQUEST['forceRegen'] !== "1"
        ) {
            $content = file_get_contents($fname);
            echo $content;
        } else {
            $content = $this->outputBuffer();
            file_put_contents($fname, $content);
            echo $content;
        }
    }

    /**
     * Generate the top level tree. It should be array of hashes, i.e.:
     * array(
     *   array('display_name' => 'example.es', 'host_object_id' => 1)),
     *   array('display_name' => 'anotherexample.es', 'host_object_id' => 2)),
     * )
     * The fields display_name and host_object_id are required
     *
     * @param $filter
     * @return mixed
     */
    public abstract function getTopLevel($filter);

    /**
     * Generate the second level tree. It should be array of hashes, i.e.:
     * array(
     *   array('display_name' => 'service 1', 'name' => 'service 1', 'service_object_id' => 1)),
     *   array('display_name' => 'service 2', 'name' => 'service 2', 'service_object_id' => 2)),
     * )
     * @param $parent_id
     * @param $filter2
     * @return mixed
     */
    public abstract function getSecondLevel($parent_id, $filter2);

    public function topLevel($id_bsm_customer, $host, $filter, $filter2)
    {
        $iterator = $this->getTopLevel($filter);
        $this->beginTopLevel();
        foreach ($iterator as $row) {
            $this->outputFirstRow($id_bsm_customer, $host, $filter2, $row);
        }
        $this->finishTopLevel();
    }

    /**
     * @param $id_bsm_customer
     * @param $host
     * @param $filter2
     * @param $row
     */
    public function outputFirstRow($id_bsm_customer, $host, $filter2, $row)
    {
        $host_object_id = $this->extractFirstHostId($row);
        $itemId = $this->getFirstRowId($host, $host_object_id, $row);
        $inUse = $this->ciInUse($itemId, $id_bsm_customer);
        $itemText = $this->getFirstRowText($row);
        $isDynamic = $this->isDynamicLoading() ? "child='1'" : '';
        $escapedItemId = str_replace('+', '%2B', $itemId);
        print("\t<item id='".$escapedItemId."' im0='server.png' im1='server.png' im2='server.png' $inUse $isDynamic text=\"".$itemText."\">\n");
        print("\t\t<userdata name='ci_type'>server</userdata>\n");
        print("\t\t<userdata name='ci_monitor'>".$itemId."</userdata>\n");
        if (!$this->isDynamicLoading()) {
            $this->secondLevel($host_object_id, $id_bsm_customer, $host, $filter2);
        }
        print("\t</item>");
    }

    /**
     * Output the second level
     * @param $host_object_id   string parent_id on the host
     * @param $id_bsm_customer  string customer
     * @param $host             string host id
     * @param $filter2          string filter for the second level
     */
    public function secondLevel($host_object_id, $id_bsm_customer, $host, $filter2)
    {
        $iterator = $this->getSecondLevel($host_object_id, $filter2);
        $this->beginSecondLevel($host_object_id, $id_bsm_customer, $host);
        foreach ($iterator as $row) {
            $monitor_id = $this->monitorId($host, $row['service_object_id'], $host_object_id);
            $metadata = $this->processSecondMetadata($id_bsm_customer, $row, $monitor_id);
            $this->outputSecondRow($metadata, $monitor_id, $row);
        }
        $this->finishSecondLevel($host_object_id, $id_bsm_customer, $host);
    }

    /**
     * @return boolean
     */
    public function isAvailable()
    {
        return $this->available;
    }

    public function close()
    {
        // todo: remove this, use db service
        if ($this->linkObsidian) {
            if ($this->linkObsidian instanceof \Obsidian\Tools\Database) {
                $this->linkObsidian->close();
            } else {
                mysqli_close($this->linkObsidian);
            }
            $this->linkObsidian = null;
        }
    }

    public function beginSecondLevel($host_object_id, $id_bsm_customer, $host)
    {
    }

    /**
     * Returns tool Id
     */
    public function getToolId()
    {
        return $this->toolParams['id'];
    }


    /**
     * Returns tool keyname - source
     */
    public function getToolKeyname()
    {
        return strtolower($this->className());
    }


    /**
     * Gets the current class name
     * @return string class name without namespace
     */
    public function className()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * Gets the integration icon. By default, it's lowercase classname
     * @return string integration icon
     */
    public function getToolIcon()
    {
        return sprintf("icon.php?i=%s", $this->getToolKeyname());
    }

    /**
     * Gets the integration text. By default it's the classname if no name is in the db
     * @return string integration text in the tree
     */
    public function getToolText()
    {
        $toolText = $this->className();
        if (isset($this->toolParams['name']) && !empty($this->toolParams['name'])) {
            $toolText = $this->toolParams['name'];
        }

        return $toolText;
    }

    /**
     * Read dynamic loading setting from the config file
     * @return bool true if we are going to show the second level immediately
     */
    public function isDynamicLoading()
    {
        return $this->toolParams['dynamic_loading'];
    }

    public function beginTopLevel()
    {
        $icon = $this->getToolIcon();
        $text = $this->getToolText();
        printf("<item id='2' im0='%s' im1='%s' im2='%s' text='%s'>\n", $icon, $icon, $icon, $text);
    }

    public function finishTopLevel()
    {
        print("\n</item>\n");
    }

    /**
     * @return mixed
     */
    public abstract function getHost();

    public function monitorId($host, $id, $parent = null)
    {
        return "${host}+${id}";
    }

    /**
     * @param $id_bsm_customer
     * @param $row
     * @param $monitor_id
     * @return array
     */
    public function processSecondMetadata($id_bsm_customer, $row, $monitor_id)
    {

        $defCapacity = $this->config['capacityDefType'];
        if ($defCapacity == "") {
            $defCapacity = 0;
        }
        $defTransaction = $this->config['transactionDefType'];
        if ($defTransaction == "") {
            $defTransaction = 1;
        }

        $inUse = $this->monitorInUse($monitor_id, $id_bsm_customer);
        $ci_type = "monitor";
        $critical = "";
        $ci_name = $row['display_name'];
        if (($this->startsWith($ci_name, "ST-")) or ($this->startsWith($ci_name, "TS-"))) {
            $ci_name = substr($ci_name, 3);
            $ci_type = "synthetictransaction";
            $critical = "\t\t\t<userdata name='ci_critical'>$defTransaction</userdata>\n";
        }
        if ($this->startsWith($ci_name, "C-")) {
            $ci_name = substr($ci_name, 2);
            $ci_type = "capacity";
            $critical = "\t\t\t<userdata name='ci_critical'>$defCapacity</userdata>\n";
        }
        if (($this->startsWith($ci_name, "DV-")) or ($this->startsWith($ci_name, "VA-"))) {
            $ci_name = substr($ci_name, 3);
            $ci_type = "availability";
            $critical = "\t\t\t<userdata name='ci_critical'>1</userdata>\n";
        }
        if (($this->startsWith($ci_name, "DNV-")) or ($this->startsWith($ci_name, "NVA-"))) {
            $ci_name = substr($ci_name, 4);
            $ci_type = "availability";
            $critical = "\t\t\t<userdata name='ci_critical'>0</userdata>\n";
        }

        return array("inUse" => $inUse, "type" => $ci_type, "critical" => $critical, "name" => $ci_name);
    }

    /**
     * @param $metadata
     * @param $monitor_id
     * @param $row
     */
    public function outputSecondRow($metadata, $monitor_id, $row)
    {
        print $this->makeSecondRow($metadata, $monitor_id, $row);
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $_REQUEST['id_bsm_customer'];
    }

    /**
     * @param $host
     * @param $host_object_id
     * @param $row
     * @return string
     */
    public function getFirstRowId($host, $host_object_id, $row)
    {
        return $host."+".$host_object_id;
    }

    public function getFirstRowText($row)
    {
        return htmlspecialchars($row['display_name'], ENT_QUOTES);
    }

    /**
     * @param $row
     * @return mixed
     */
    public function extractFirstHostId($row)
    {
        return $row['host_object_id'];
    }

    /**
     * @param $metadata
     * @param $monitor_id
     * @param $row
     */
    public function makeSecondRow($metadata, $monitor_id, $row)
    {
        list ($t, $name, $critical, $inUse) = array(
            $metadata['type'],
            $metadata['name'],
            $metadata['critical'],
            $metadata['inUse'],
        );
        $out = sprintf("\t\t<item id='%s' im0='%s.png' im1='%s.png' im2='%s.png' %s text=\"%s\">
\t\t\t<userdata name='ci_tool'>%d</userdata>
\t\t\t<userdata name='ci_type'>%s</userdata>\n%s
\t\t\t<userdata name='ci_monitor'>%s</userdata>
",
            $monitor_id, $t, $t, $t, $inUse, htmlspecialchars($name, ENT_QUOTES), $this->getToolId(), $t, $critical,
            $monitor_id);
        //$monitor_id, $t, $t, $t, $inUse, str_replace('"', "&quot;", $name), $this->getToolId(), $t, $critical, $monitor_id);
        if (isset($row['varvalue']) && $row['varvalue'] != '') {
            $out .= "\t\t\t<userdata name='monitor_result'>".$row['varvalue']."</userdata>\n";
        }

        return $out."\t\t</item>\n";
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function outputBuffer()
    {
        ob_start(); // Start output buffering

        echo("<tree id=\"".$this->rootId."\">\n");
        if ($this->rootId && $this->isDynamicLoading()) {
            $this->dynamicSecondLevel();
        } else {
            // todo: user request object inject instead of direct $_REQUEST and $_GET
            $this->topLevel($this->getCustomer(), $this->getHost(), $_GET['filter'], $_GET['filter2']);
        }
        echo("</tree>\n");

        $content = ob_get_contents(); // Store buffer in variable
        ob_end_clean();

        return $content; // End buffering and clean up
    }

    /**
     * You can inherit this method when you need to initialize some resources on the class startup.
     * It's best if you free them in the {@link freeResources} method.
     *
     * @inheritdoc
     */
    public function initializeResources()
    {

    }

    /**
     * Free the resources you do not need when class is going to be destroyed. Speeds up the software, frees resources, improves stability
     *
     * @inheritdoc
     */
    public function freeResources()
    {

    }

    public function getSourceTools()
    {
        $data = array();
        if ($this->linkObsidian) {
            $sql2 = "SELECT * FROM bsm_sourcetool";
            $res2 = mysqli_query($this->linkObsidian, $sql2);
            while ($row = mysqli_fetch_array($res2)) {
                $data[$row['source']] = $row;
            }
        }

        return $data;
    }

    protected function dynamicSecondLevel()
    {
        // todo: user request object inject instead of direct $_REQUEST and $_GET
        list($host, $host_object_id) = preg_split('/([\s\+]|%2B)/', $this->rootId, 2);
        $this->secondLevel($host_object_id, $this->getCustomer(), $host, $_GET['filter2']);
    }

    /**
     * @param $host_object_id  string parent_id on the host
     * @param $id_bsm_customer string customer
     * @param $host            string host id
     */
    protected function finishSecondLevel($host_object_id, $id_bsm_customer, $host)
    {
    }
}