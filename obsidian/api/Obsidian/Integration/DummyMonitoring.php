<?php

namespace Obsidian\Integration;

use Obsidian\Exception\ObsidianException;
use Obsidian\Type;

/**
 * Example Implementation of multilevel tree in Monitoring
 */

/**
 * Class DummyMonitoring
 * @package Obsidian\Integration
 */
class DummyMonitoring extends AbstractIntegrator
{
    /**
     * @var int
     */
    var $dummy_toolid;

    /**
     * @var string
     */
    var $dummy_host;

    /**
     * @var string
     */
    var $dummy_icon;

    /**
     * @var string
     */
    var $dummy_textlabel;

    /**
     * @var string
     */
    var $dummy_top_file;

    /**
     * @var string
     */
    var $dummy_children_file;

    /**
     * DummyMonitoring constructor.
     * @param $config
     */
    public function __construct($config)
    {
        parent::__construct($config);
        // here we can init the some vars
        // todo: implement some data provider and inject it
        $this->dummy_top_file = (isset($config['dummy_top_monitoring_file']) && file_exists($config['dummy_top_monitoring_file'])) ? $config['dummy_top_monitoring_file'] : '/opt/ozona/obsidian/api/Obsidian/Integration/Data/dummy_top_monitoring.json';
        $this->dummy_children_file = (isset($config['dummy_children_monitoring_file']) && file_exists($config['dummy_children_monitoring_file'])) ? $config['dummy_children_monitoring_file'] : '/opt/ozona/obsidian/api/Obsidian/Integration/Data/dummy_children_monitoring.json';

        $this->dummy_host = ($config['dummy_host'] && !empty($config['dummy_host'])) ? $config['dummy_host'] : 'dummyserver';
        $this->dummy_toolid = ($config['dummy_toolid'] && !empty($config['dummy_toolid'])) ? $config['dummy_toolid'] : 2001;
        $this->dummy_icon = ($config['dummy_icon'] && !empty($config['dummy_icon'])) ? $config['dummy_icon'] : 'dummymonitoring';
        $this->dummy_textlabel = ($config['dummy_textlabel'] && !empty($config['dummy_textlabel'])) ? $config['dummy_textlabel'] : 'DummyMonitoring';
    }

    /**
     * Returns the values for the monitor
     * @param $source string parent id
     * @param $info array monitor info
     * @param $monitor_type string monitor type
     * @param $date string
     * @return array parsed values for the monitor
     */
    public function getParsedValue($source, $info, $monitor_type, $date = null)
    {
        $data = $this->getJson($this->dummy_children_file);
        $output = 100;
        $state = 0;
        foreach ($data as $server) {
            foreach ($server as $monitor) {
                if ($monitor['id'] == $info['ci_monitor']) {
                    $output = $monitor['output'];
                    $state = $monitor['state'];
                }
            }
        }
        $res = array(
            'OUTPUT' => $output,
            'STATE' => $state,
            'start_time' => date("Y-m-d H:i:s"),
            'end_time' => date("Y-m-d H:i:s"),
        );
        $val = $this->calculateParsedValue($info, $res);
        $res['valor'] = $val;
        $res['valor'] = min(100, ponderar($val, $res['STATE'], $info, $monitor_type));

        return $res;
    }

    /**
     * Helper function to retrieve the data
     */
    public function getJson($json_file = '')
    {
        if (!file_exists($json_file)) {
            throw new ObsidianException('File not found: '.$json_file);
        }

        $file_content = file_get_contents($json_file);
        $json = json_decode($file_content, true);

        if ($json === null) {
            throw new ObsidianException('Bad JSON format in file: '.$json_file);
        }

        return $json;
    }

    /**
     * Function returns the host of the service
     */
    public function getHost()
    {
        return $this->dummy_host;
    }

    /**
     * Returns tool Id
     */
    public function getToolId()
    {
        return $this->dummy_toolid;
    }

    /**
     * Gets the integration icon.
     * @return string integration icon
     */
    public function getToolIcon()
    {
        return sprintf("icon.php?i=%s", $this->dummy_icon);
    }

    /**
     * Gets the integration text.
     * @return string integration text in the tree
     */
    public function getToolText()
    {
        return $this->dummy_textlabel;
    }

    /**
     *  Returns array used to print the first level nodes:
     * array(
     *      array('display_name' => 'some example 1', 'host_object_id' => 1)),
     *      array('display_name' => 'some example 2', 'host_object_id' => 2)),
     *      array('display_name' => 'some example 3', 'host_object_id' => 3))
     * )
     * =====================================================
     *
     * @param mixed $filter
     * @return array
     */
    public function getTopLevel($filter = null)
    {
        $data = $this->getJson($this->dummy_top_file);
        $servers = array();
        $filter = strtolower($filter);
        foreach ($data as $row) {

            if ($filter && stripos($row['name'], $filter) === false) {
                continue;
            }

            $servers[] = array(
                'host_object_id' => $row['id'],
                'display_name' => $row['name'],
            );

        }
        usort($servers, function ($a, $b) {
            $al = strtolower($a['display_name']);
            $bl = strtolower($b['display_name']);
            if ($al == $bl) {
                return 0;
            }

            return $al < $bl ? -1 : 1;
        });

        return $servers;
    }

    /**
     * Returns the array for the second level nodes:
     * array(
     *      array('display_name' => 'some example 1', 'service_object_id' => 1)),
     *      array('display_name' => 'some example 2', 'service_object_id' => 2)),
     *      array('display_name' => 'some example 3', 'service_object_id' => 3))
     * )
     * @param null $parent_id
     * @param null $filter
     * @return array
     */
    public function getSecondLevel($parent_id = null, $filter = null)
    {
        $data = $this->getJson($this->dummy_children_file);
        $data = $data[$parent_id];
        $monitors = array();
        foreach ($data as &$row) {
            if ($row['type'] == Type::AVAILABILITY) {
                $name = Type::PREFIX_AVAILABILITY.$row['name'];
            } elseif ($row['type'] == Type::CAPACITY) {
                $name = Type::PREFIX_CAPACITY.$row['name'];
            } elseif ($row['type'] == Type::SYNTH_TRANSACTION) {
                $name = Type::PREFIX_SYNTH_TRANSACTION.$row['name'];
            } else {
                $name = $row['name'];
            }
            if ($filter != null) {
                if (strpos(strtolower($name), strtolower($filter)) !== false) {
                    $monitors[] = array(
                        'service_object_id' => $row['id'],
                        'display_name' => $name,
                    );
                }
            } else {
                $monitors[] = array(
                    'service_object_id' => $row['id'],
                    'display_name' => $name,
                );
            }
        }

        return $monitors;
    }

    /**
     * Builds the list of servers with default monitors and saves them to a file
     * @param $servers array list of server names and templates:
     * array(array('name'=> 'server_name', 'template' => 'linuxServer'))
     */
    public function setDummyServers($servers = null)
    {
        $data = array();
        $monitors = array();
        if (!empty($servers)) {
            $i = 1;
            foreach ($servers as $entry) {
                $server = array(
                    "name" => $entry['name'],
                    "id" => (string)(1030 + $i),
                    "type" => "server",
                );
                $data[] = $server;
                $monitors[$server['id']] = $this->buildMonitorsFromTemplate($server['id'], $entry['template']);
                $i++;
            }
        } else {
            for ($i = 1; $i <= 3; $i++) {
                $server = array(
                    "name" => "Server ".$i,
                    "id" => (string)(1030 + $i),
                    "type" => "server",
                );
                $data[] = $server;
                $monitors[$server['id']] = $this->buildMonitorsFromTemplate($server['id'], "linuxServer");
                $i++;
            }
        }
        file_put_contents($this->dummy_top_file, json_encode($data));
        file_put_contents($this->dummy_children_file, json_encode($monitors));
    }

    /**
     * Returns the list of monitors loaded from the template
     * @param $server_id string server id
     * @param $template string template name
     * @return array
     */
    public function buildMonitorsFromTemplate($server_id, $template)
    {
        $data = $this->getJson(__DIR__."/Templates/DummyTemplates.json")[$template];
        $monitors = array();
        $i = 1;
        foreach ($data as $monitor) {
            $monitors[] = $this->buildMonitor($server_id.$i, $monitor['name'], $monitor['type'], 100, 0);
            $i++;
        }

        return $monitors;
    }

    /**
     * Builds a monitor
     * @param $id string monitor id
     * @param $name string monitor name
     * @param $type string monitor type
     * @param $output string monitor output value
     * @param $state int monitor state
     * @return array
     */
    public function buildMonitor($id, $name, $type, $output, $state)
    {
        return array(
            "id" => $id,
            "name" => $name,
            "type" => $type,
            "output" => $output,
            "state" => $state,
        );
    }

    /**
     * Saves the list of monitors to a file
     * @param $server_id string server id
     * @param $info array array of monitors with their data:
     * array(array('name'=> 'monitor_name', 'type' => 'monitor_type'))
     */
    public function setDummyServerMonitors($server_id, $info)
    {
        $data = $this->getJson($this->dummy_children_file);
        $i = 1;
        foreach ($info as &$monitor) {
            $monitor['id'] = $server_id.$i;
            $monitor['output'] = 100;
            $monitor['state'] = 0;
            $i++;
        }
        $data[$server_id] = $info;
        file_put_contents($this->dummy_children_file, json_encode($data));
    }

    /**
     * @param $values array with values for each monitor:
     * array(array('id', 'output', 'state'))
     */
    public function setValues($values)
    {
        $data = $this->getJson($this->dummy_children_file);
        foreach ($data as &$monitors) {
            foreach ($monitors as &$monitor) {
                foreach ($values as $value) {
                    if ($monitor['id'] == $value['id']) {
                        $monitor['output'] = $value['output'];
                        $monitor['state'] = $value['state'];
                    }
                }
            }
        }
        file_put_contents($this->dummy_children_file, json_encode($data));
    }
}
