<?php

namespace Obsidian\Integration;

/**
 * Example Implementation of multilevel tree in Monitoring
 */
class TemplateMonitoring extends AbstractIntegrator
{
    /**
     * @var int
     */
    var $dummy_toolid;

    /**
     * @var string
     */
    var $dummy_host;

    /**
     * @var string
     */
    var $dummy_top_file;

    /**
     * @var string
     */
    var $dummy_children_file;

    /**
     * TemplateMonitoring constructor.
     * @param $config
     */
    public function __construct($config)
    {
        parent::__construct($config);
        // here we can init the some vars
        $this->dummy_toolid = ($config['dummy_toolid'] && !empty($config['dummy_toolid']))
            ? $config['dummy_toolid'] : 10023;
        $this->dummy_host = ($config['dummy_host'] && !empty($config['dummy_host']))
            ? $config['dummy_host'] : 'www.some_dummy_host.com';
    }

    /**
     * Returns the values for the monitor
     * @param $source string parent id
     * @param $info array monitor info
     * @param $monitor_type string monitor type
     * @param $date string
     * @return array parsed values for the monitor
     */
    public function getParsedValue($source, $info, $monitor_type, $date = null)
    {
        // Needs to be implemented!
        $res = array(
            'OUTPUT' => 100,
            'STATE' => 0,
            'start_time' => date("Y-m-d H:i:s"),
            'end_time' => date("Y-m-d H:i:s"),
            'valor' => 100,
        );

        return $res;
    }

    /**
     * Function returns the host of the service
     */
    public function getHost()
    {
        return $this->dummy_host;
    }

    /**
     *  Returns array used to print the first level nodes:
     * array(
     *      array('display_name' => 'some example 1', 'host_object_id' => 1)),
     *      array('display_name' => 'some example 2', 'host_object_id' => 2)),
     *      array('display_name' => 'some example 3', 'host_object_id' => 3))
     * )
     * =====================================================
     *
     * @param mixed $filter
     * @return array
     */
    public function getTopLevel($filter = null)
    {
        // Needs to be implemented!
        $data = array();
        $data[] = array(
            'display_name' => 'server 1',
            'host_object_id' => 10,
        );
        $data[] = array(
            'display_name' => 'server 2',
            'host_object_id' => 11,
        );

        return $data;
    }

    /**
     * Returns the array for the second level nodes:
     * array(
     *      array('display_name' => 'some example 1', 'service_object_id' => 1)),
     *      array('display_name' => 'some example 2', 'service_object_id' => 2)),
     *      array('display_name' => 'some example 3', 'service_object_id' => 3))
     * )
     * @param null $parent_id
     * @param null $filter
     * @return array
     */
    public function getSecondLevel($parent_id = null, $filter = null)
    {
        //Needs to be implemented!
        $data = array();
        $data[] = array(
            'display_name' => 'monitor 1',
            'host_object_id' => 101,
        );
        $data[] = array(
            'display_name' => 'monitor 2',
            'host_object_id' => 111,
        );

        return $data;
    }
}