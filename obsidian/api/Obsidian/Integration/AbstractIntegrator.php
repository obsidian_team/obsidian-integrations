<?php

namespace Obsidian\Integration;

/**
 * Class AbstractIntegrator
 * @package Obsidian\Integration
 */
abstract class AbstractIntegrator extends ObsidianTreeBuilder
{
    /**
     * @param $source
     * @param $info
     * @param $monitor_type
     * @param null $date
     * @return mixed
     */
    public abstract function getParsedValue($source, $info, $monitor_type, $date = null);

    /**
     * @param $info
     * @param $row
     * @return float|int|mixed|null
     */
    function calculateParsedValue($info, $row)
    {
        //Default Value
        $resultado = $row['STATE'];

        //If there are not max and min values we use 100,0

        if (is_null($info['monitor_max']) || ($info['monitor_max'] === "")) {
            $info['monitor_max'] = 100;
        }
        if (is_null($info['monitor_min']) || ($info['monitor_min'] === "")) {
            $info['monitor_min'] = 0;
        }
        //If thesre's no info about positivr use from config
        if (is_null($info['monitor_positive']) || ($info['monitor_positive'] === "")) {
            $info['monitor_positive'] = $this->config['checksdef']['positive'];
        }

        //If ESTADO returned is 3, then use config value for (unknown)
        // todo: use constant instead of 3
        if ($resultado == 3) {
            return $this->config['discrete']['UNKN'];
        }
        //If we want discrete values and we have theOUTPUT field (or the one configured in 'monitor_ftq') we can parse
        if ((!$this->config['forceDiscrete']) && (strtoupper($info['monitor_ftq']) == 'OUTPUT')) {
            //Parsing atributes
            //‘monitor_ftq’: field to query
            //‘monitor_result’: long result or discrete value in sscanf suitable format
            //‘monitor_vir’: value in result, which value of the result should be used?

            $valor = sscanf($row[strtoupper($info['monitor_ftq'])], $info['monitor_result']);
            $tmp_result = $valor[$info['monitor_vir'] - 1];
            echo("\n\nvalor: ".sscanf($row[strtoupper($info['monitor_ftq'])], $info['monitor_result'])."\n\n");
            echo("\n\nregexp: ".$row[strtoupper($info['monitor_ftq'])]." --- ".$info['monitor_result']."= ".$valor[0]."\n\n");
            if (is_null($tmp_result)) {
                $tmp_result = $resultado;
            } else {
                //Parsed OK
                //If there's a max configured and the parsed value is over, we use the parsed value as maximum
                //Just for "giga" values
                $tmp_result = min($info['monitor_max'], $tmp_result);
            }
            $tmp_result = $this->normalize($tmp_result, $info['monitor_max'], $info['monitor_min'],
                $info['monitor_positive']);

        } else { //Using ESTADO as value
            $tmp_result = $resultado;
            //If using ESTADO, always is 0,1,2 and negative
            $tmp_result = 100 - $this->normalize($tmp_result, 2, 0, 1);
            switch ($tmp_result) {
                case 100:
                    $tmp_result = $this->config['discrete']['OK'];
                    break;
                case 50:
                    $tmp_result = $this->config['discrete']['WARN'];
                    break;
                default:
                    $tmp_result = $this->config['discrete']['CRIT'];
                    break;
            }
        }
        if (!is_null($tmp_result)) {
            return $tmp_result;
        }

        return $resultado;
    }

    /**
     * @param $value
     * @param int $max
     * @param int $min
     * @param $positive
     * @return float|null
     */
    function normalize($value, $max, $min, $positive)
    {
        //Default return value
        //echo("\n\nvalor: ".$value."\n\n");

        //Get values from config if not supplied
        // todo: add config validation, check why not used is_int()?
        if (is_null($max) || ($max === "")) {
            $max = $this->config['checksdef']['max'];
        }
        if (is_null($min) || ($min === "")) {
            $min = $this->config['checksdef']['min'];
        }
        if (is_null($positive) || ($positive === "")) {
            // todo: not used anywhere in code, check usage
            $positive = $this->config['checksdef']['positive'];
        }
        //Get range dimension
        $q = abs($max - $min);
        if ($q == 0) { //Has to be >0, get from values from config if supplied are not ok
            $max = $this->config['checksdef']['max'];
            $min = $this->config['checksdef']['min'];
            // todo: what if max and min matches in config? still 0
            $q = abs($max - $min);
            if ($q === 0) {
                // todo: add exception?
                return null;
            }
        }

        //Normalize between [0,100]
        $retorno = (($value - $min) / $q) * 100;
        //If monitor is negative substract from 100
        //if ($positive > 1) $retorno = 100 - $retorno;
        //Get sure we are in [0,100]
        $retorno = max(0, $retorno);
        $retorno = min(100, $retorno);

        return $retorno;
    }
}