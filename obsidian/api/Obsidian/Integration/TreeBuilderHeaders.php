<?php

namespace Obsidian\Integration;

trait TreeBuilderHeaders
{
    public function sendHeaders()
    {
        if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            header("Content-type: application/xhtml+xml");
        } else {
            header("Content-type: text/xml");
            header("Expires: Tue, 01 Jul 2001 06:00:00 GMT");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
        }
        echo("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    }
}