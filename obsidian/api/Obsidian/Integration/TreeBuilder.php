<?php

namespace Obsidian\Integration;

interface TreeBuilder
{
    public function outputTree();
}