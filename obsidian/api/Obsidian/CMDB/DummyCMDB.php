<?php
/**
 * Example Implementation of multilevel tree in CMDB
 */

namespace Obsidian\CMDB;

use Obsidian\Exception\ObsidianException;
use Obsidian\Type;

/**
 * Class DummyCMDB
 * @package Obsidian\CMDB
 */
class DummyCMDB extends AbstractCMDB
{
    /**
     * @var int
     */
    var $dummy_toolid;

    /**
     * @var string
     */
    var $dummy_host;

    /**
     * @var string
     */
    var $dummy_icon;

    /**
     * @var string
     */
    var $dummy_textlabel;

    /**
     * @var string
     */
    var $dummy_top_file;

    /**
     * @var string
     */
    var $dummy_children_file;

    /**
     * @var
     */
    var $services;

    /**
     * @var int
     */
    var $unused_leaf;

    /**
     * DummyCMDB constructor.
     * @param $config
     */
    public function __construct($config)
    {
        parent::__construct($config);
        // here we can init the some vars
        $this->dummy_top_file = (isset($config['dummy_top_CMDB_file']) && file_exists($config['dummy_top_CMDB_file'])) ? $config['dummy_top_CMDB_file'] : '/opt/ozona/obsidian/api/Obsidian/CMDB/Data/dummy_top_CMDB.json';
        $this->dummy_children_file = (isset($config['dummy_children_CMDB_file']) && file_exists($config['dummy_children_CMDB_file'])) ? $config['dummy_children_CMDB_file'] : '/opt/ozona/obsidian/api/Obsidian/CMDB/Data/dummy_children_CMDB.json';

        $this->dummy_host = ($config['dummycmdb_host'] && !empty($config['dummycmdb_host'])) ? $config['dummycmdb_host'] : 'dummycmdb';
        $this->dummy_toolid = ($config['dummycmdb_toolid'] && !empty($config['dummycmdb_toolid'])) ? $config['dummycmdb_toolid'] : 2002;
        $this->dummy_icon = ($config['dummycmdb_icon'] && !empty($config['dummycmdb_icon'])) ? $config['dummycmdb_icon'] : 'dummycmdb';
        $this->dummy_textlabel = ($config['dummycmdb_textlabel'] && !empty($config['dummycmdb_textlabel'])) ? $config['dummycmdb_textlabel'] : 'DummyCMDB';
        $this->unused_leaf = 0;
    }

    /**
     * Function returns the host of the service
     */
    public function getHost()
    {
        return $this->dummy_host;
    }

    /**
     * Returns tool Id
     */
    public function getToolId()
    {
        return $this->dummy_toolid;
    }

    /**
     * Gets the integration icon.
     * @return string integration icon
     */
    public function getToolIcon()
    {
        return sprintf("icon.php?i=%s", $this->dummy_icon);
    }

    /**
     * Gets the integration text.
     * @return string integration text in the tree
     */
    public function getToolText()
    {
        return $this->dummy_textlabel;
    }

    /**
     * This is example implementation of the abstract method getTopLevel
     * =====================================================
     *  The method should return array used to print the first level nodes :
     * array(
     *      array('display_name' => 'some example 1', 'host_object_id' => 1)),
     *      array('display_name' => 'some example 2', 'host_object_id' => 2)),
     *      array('display_name' => 'some example 3', 'host_object_id' => 3))
     * )
     * =====================================================
     *
     * @param mixed $filter
     * @return array
     */
    public function getTopLevel($filter = null)
    {
        $data = $this->getJson($this->dummy_top_file);
        $services = array();
        $filter = strtolower($filter);
        foreach ($data as $row) {
            if ($filter && stripos($row['name'], $filter) === false) {
                continue;
            }

            $services[] = array(
                'host_object_id' => $row['id'],
                'display_name' => $row['name'],
                'type' => $row['type'],
            );
        }
        usort($services, function ($a, $b) {
            $al = strtolower($a['display_name']);
            $bl = strtolower($b['display_name']);
            if ($al == $bl) {
                return 0;
            }

            return $al < $bl ? -1 : 1;
        });

        return $services;
    }

    /**
     *
     */
    public function beginTopLevel()
    {
        $icon = $this->getToolIcon();
        $text = $this->getToolText();
        printf("<item id='1' im0='%s' im1='%s' im2='%s' text='%s'>\n", $icon, $icon, $icon, $text);
    }

    /**
     * returns array for the second level nodes
     * ================================================
     * This method is not used in CMDB, but needs to be
     * initialized since it is an abstract method from the
     * parent class
     *
     * @deprecated Unused
     */
    public function getSecondLevel($parent_id = null, $filter = null)
    {
    }
    // =================================================================

    /**
     * returns the array for the children nodes
     * @param null $parent_id
     * @param null $filter
     * @return mixed
     */
    public function getChildrenLevel($parent_id, $filter = null)
    {
        $data = $this->getJson($this->dummy_children_file);
        if ($filter != null) {
            $this->filterChildren($data[$parent_id], $filter);
        }

        return $data[$parent_id];
    }

    /**
     * @param $parents
     * @param $filter
     */
    public function filterChildren(&$parents, $filter)
    {
        $res = array();
        $filter = strtolower($filter);
        foreach ($parents as &$row) {
            if (!isset($row['children']) || empty($row['children'])) {
                if (strpos(strtolower($row['name']), $filter) === false) {
                    continue;
                }
            }
            $this->filterChildren($row['children'], $filter);
            $res[] = $row;
        }
        $parents = $res;
    }

    /**
     * Prints the XML for an item of the top level
     * @param $id_bsm_customer
     * @param $host
     * @param $filter2
     * @param $row
     */
    public function outputFirstRow($id_bsm_customer, $host, $filter2, $row)
    {
        $type = $row['type'];
        $host_object_id = $this->extractFirstHostId($row);
        $itemId = $this->getFirstRowId($host, $host_object_id, $row);
        $inUse = $this->ciInUse($itemId, $id_bsm_customer);
        $itemText = $this->getFirstRowText($row);
        $isDynamic = $this->isDynamicLoading() ? "child='1'" : '';
        $escapedItemId = str_replace('+', '%2B', $itemId);
        print("\t<item id='".$escapedItemId."' im0='".$type.".png' im1='".$type.".png' im2='".$type.".png' $inUse $isDynamic text=\"".$itemText."\">\n");
        print("\t\t<userdata name='ci_type'>".$type."</userdata>\n");
        print("\t\t<userdata name='ci_monitor'>".$itemId."</userdata>\n");
        if (!$this->isDynamicLoading()) {
            $data = $this->getChildrenLevel($host_object_id, $filter2);
            $this->childrenLevel($host_object_id, $id_bsm_customer, $host, $data);
        }
        print("\t\t<userdata name='org_text'>".$itemText."</userdata>\n");
        print("\t\t<userdata name='unused_leaf'>".$this->unused_leaf."</userdata>\n");
        $this->unused_leaf = 0;
        print("\t</item>");
    }

    /**
     * Outputs the second level
     * @param $host_object_id   string parent_id on the host
     * @param $id_bsm_customer  string customer
     * @param $host             string host id
     * @param $data             array children data
     */
    public function childrenLevel($host_object_id, $id_bsm_customer, $host, $data)
    {
        $this->beginSecondLevel($host_object_id, $id_bsm_customer, $host);
        $metadataBase = [
            'host_object_id' => $host_object_id,
            'id_bsm_customer' => $id_bsm_customer,
            'host' => $host,
        ];
        foreach ($data as $row) {
            $monitor_id = $this->monitorId($host, $row['id'], $host_object_id);
            $metadata = $metadataBase + $this->processSecondMetadata($id_bsm_customer, $row, $monitor_id);
            $this->outputChildren($metadata, $monitor_id, $row);
        }
        $this->finishSecondLevel($host_object_id, $id_bsm_customer, $host);
    }

    /**
     * Returns the data needed to print the children's XML
     * @param $id_bsm_customer
     * @param $row
     * @param $monitor_id
     * @return array
     */
    public function processSecondMetadata($id_bsm_customer, $row, $monitor_id)
    {
        $inUse = $this->ciInUse($monitor_id, $id_bsm_customer);
        $ci_type = $row['type'];
        $critical = "\t\t\t<userdata name='ci_critical'>1</userdata>\n";
        $ci_name = $row['name'];

        return ['inUse' => $inUse, 'type' => $ci_type, 'critical' => $critical, 'name' => $ci_name];
    }

    /**
     * Prints the XML for an item of the children
     * @param $metadata
     * @param $monitor_id
     * @param $row
     */
    public function outputChildren($metadata, $monitor_id, $row)
    {
        list ($t, $name, $critical, $inUse) = array(
            $metadata['type'],
            $metadata['name'],
            $metadata['critical'],
            $metadata['inUse'],
        );
        $tool_id = $this->getToolId();
        if ($t == 'dependencies2') {
            $monitor_id = explode("+", $monitor_id)[1];
            $tool_id = '';
        }
        if ($inUse == '') {
            $this->unused_leaf++;
        }
        // todo: don't use globals, inject request
        if ($_GET['inUseOnly'] == 'false' || $inUse == '') {
            $out = sprintf("\t\t<item id='%s' im0='%s.png' im1='%s.png' im2='%s.png' %s text=\"%s\">
            \t\t\t<userdata name='ci_tool'>%d</userdata>
            \t\t\t<userdata name='ci_type'>%s</userdata>\n%s
            \t\t\t<userdata name='ci_monitor'>%s</userdata>
            ", $monitor_id, $t, $t, $t, $inUse, htmlspecialchars($name, ENT_QUOTES), $tool_id, $t, $critical,
                $monitor_id);
            print $out;
            if (isset($row['children'])) {
                $this->childrenLevel($metadata['host_object_id'], $metadata['id_bsm_customer'], $metadata['host'],
                    $row['children']);
            }
            print "\t\t</item>\n";
        }
    }

    /**
     * Helper function to retrieve the data
     */
    public function getJson($json_file = '')
    {
        if (!file_exists($json_file)) {
            throw new ObsidianException('File not found: '.$json_file);
        }

        $file_content = file_get_contents($json_file);
        $json = json_decode($file_content, true);

        if ($json === null) {
            throw new ObsidianException('Bad JSON format in file: '.$json_file);
        }

        return $json;
    }

    /**
     * @param $services array list of services names
     */
    public function setDummyServices($services)
    {
        $data = array();
        $structure = array();
        $id = 1550;
        if (!empty($services)) {
            foreach ($services as $name) {
                $service = array(
                    'name' => $name,
                    'id' => (string)$id,
                    'type' => Type::SERVICE,
                );
                $data[] = $service;
                $structure[$id] = $this->buildDefaultStructure($id);
                $id++;
            }
        } else {
            for ($i = 1; $i <= 3; $i++) {
                $service = array(
                    'name' => 'Service '.$i,
                    'id' => (string)$id,
                    'type' => Type::SERVICE,
                );
                $data[] = $service;
                $structure[$id] = $this->buildDefaultStructure($id);
                $id++;
            }
        }
        file_put_contents($this->dummy_top_file, json_encode($data));
        file_put_contents($this->dummy_children_file, json_encode($structure));
    }

    /**
     * @param $service_id string
     * @param $processes array list of names for the processes
     */
    public function setDummyServiceBusinessProcesses($service_id, $processes)
    {
        $data = $this->getJson($this->dummy_children_file);
        foreach ($data[$service_id] as &$d) {
            if ($d['type'] == Type::BUSINESSPROCESS) {
                $i = 1;
                $procs = array();
                foreach ($processes as $p) {
                    $process = array();
                    $process['id'] = $d['id'].$i;
                    $process['name'] = $p;
                    $process['type'] = Type::BUSINESSPROCESS;
                    $i++;
                    $procs[] = $process;
                }
                $d['children'] = $procs;
            }
        }
        file_put_contents($this->dummy_children_file, json_encode($data));
    }

    /**
     * @param $service_id
     * @param $applications array list of names for the applications
     */
    public function setDummyServiceApplications($service_id, $applications)
    {
        $data = $this->getJson($this->dummy_children_file);
        foreach ($data[$service_id] as &$d) {
            if ($d['type'] == Type::SUBSERVICE && isset($d['children'])) {
                foreach ($d['children'] as &$child) {
                    if ($child['type'] == Type::APPLICATION) {
                        $i = 1;
                        $apps = array();
                        foreach ($applications as $a) {
                            $app = array();
                            $app['id'] = $child['id'].$i;
                            $app['name'] = $a;
                            $app['type'] = Type::APPLICATION;
                            $i++;
                            $apps[] = $app;
                        }
                        $child['children'] = $apps;
                    }
                }
            }
        }
        file_put_contents($this->dummy_children_file, json_encode($data));
    }

    /**
     * @param $service_id
     * @param $servers array list of names for the servers
     */
    public function setDummyServiceServers($service_id, $servers)
    {
        $data = $this->getJson($this->dummy_children_file);
        foreach ($data[$service_id] as &$d) {
            if ($d['type'] == Type::SUBSERVICE && isset($d['children'])) {
                foreach ($d['children'] as &$child) {
                    if ($child['type'] == Type::SERVER) {
                        $i = 1;
                        $servs = array();
                        foreach ($servers as $s) {
                            $serv = array();
                            $serv['id'] = $child['id'].$i;
                            $serv['name'] = $s;
                            $serv['type'] = Type::SERVER;
                            $i++;
                            $servs[] = $serv;
                        }
                        $child['children'] = $servs;
                    }
                }
            }
        }
        file_put_contents($this->dummy_children_file, json_encode($data));
    }

    /**
     * @param $service_id
     * @param $dependencies array list of names for the dependencies
     */
    public function setDummyServiceDependencies($service_id, $dependencies)
    {
        $data = $this->getJson($this->dummy_children_file);
        foreach ($data[$service_id] as &$d) {
            if ($d['type'] == Type::DEPENDENCIES) {
                $i = 1;
                $deps = array();
                foreach ($dependencies as $de) {
                    $dep = array();
                    $dep['id'] = $d['id'].$i;
                    $dep['name'] = $de;
                    $dep['type'] = Type::DEPENDENCIES;
                    $i++;
                    $deps[] = $dep;
                }
                $d['children'] = $deps;
            }
        }
        file_put_contents($this->dummy_children_file, json_encode($data));
    }

    /**
     * Builds the default structure for a service
     * @param $parent_id
     * @return array
     */
    public function buildDefaultStructure($parent_id)
    {
        $structure = array();
        $structure[] = array(
            "id" => $parent_id. 1,
            "name" => "Business processes",
            "type" => Type::BUSINESSPROCESS,
            "children" => array(),
        );
        $structure[] = array(
            "id" => $parent_id. 2,
            "name" => "IT Subservices",
            "type" => Type::SUBSERVICE,
            "children" => array(),
        );
        $children = array();
        $children[] = array(
            "id" => $parent_id. 3 . 1,
            "name" => "Service Desk indicators",
            "type" => Type::SUBSERVICE,
            "children" => array(),
        );
        $children[] = array(
            "id" => $parent_id. 3 . 2,
            "name" => "Applications",
            "type" => Type::APPLICATION,
            "children" => array(),
        );
        $children[] = array(
            "id" => $parent_id. 3 . 3,
            "name" => "Servers and infrastructure",
            "type" => Type::SERVER,
            "children" => array(),
        );
        $structure[] = array(
            "id" => $parent_id. 3,
            "name" => "Common",
            "type" => Type::SUBSERVICE,
            "children" => $children,
        );
        $structure[] = array(
            "id" => $parent_id. 4,
            "name" => "Dependencies",
            "type" => Type::DEPENDENCIES,
            "children" => array(),
        );

        return $structure;
    }

    /**
     *
     */
    protected function dynamicSecondLevel()
    {
        list($host, $host_object_id) = preg_split('/([\s\+]|%2B)/', $this->rootId, 2);
        $data = $this->getChildrenLevel($host_object_id, $_GET['filter2']);
        $this->childrenLevel($host_object_id, $this->getCustomer(), $host, $data);
    }
}
