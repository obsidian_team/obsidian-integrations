<?php
/**
 * ==========================================================================
 * You need to write the code needed to print the XML for the children nodes.
 * We have an implementation example in the class DummyCMDB.
 * ==========================================================================
 */
namespace Obsidian\CMDB;

/**
 * Class TemplateCMDB
 * @package Obsidian\CMDB
 */
class TemplateCMDB extends AbstractCMDB
{
    /**
     * @var int
     */
    var $dummy_toolid;

    /**
     * @var string
     */
    var $dummy_host;

    /**
     * TemplateCMDB constructor.
     * @param $config
     */
    public function __construct($config)
    {
        parent::__construct($config);
        // here we can init the some vars
        $this->dummy_toolid = ($config['dummy_toolid'] && !empty($config['dummy_toolid']))
            ? $config['dummy_toolid'] : 1022;
        $this->dummy_host = ($config['dummy_host'] && !empty($config['dummy_host']))
            ? $config['dummy_host'] : 'www.some_dummy_host.com';
    }

    /**
     * Now we need to integrate the abstract function of ObsidianTreeBuilder
     */

    /**
     * Function returns the host of the service
     */
    public function getHost()
    {
        return $this->dummy_host;
    }


    /**
     * This is example implementation of the abstract method getTopLevel  
     * =====================================================
     *  The method should return array used to print the first level nodes :
     * array(
     *      array('display_name' => 'some example 1', 'host_object_id' => 1)),
     *      array('display_name' => 'some example 2', 'host_object_id' => 2)),
     *      array('display_name' => 'some example 3', 'host_object_id' => 3))
     * )
     * =====================================================
     *
     * @param mixed $filter
     * @return array
     */
    public function getTopLevel($filter = null)
    {
        $services = array();
        // we can use helper functions to get the array
        $services_json = $this->getTopLevelServices();
        $services_array = json_decode($services_json, true);
        foreach ($services_array as $service_id => $service_name) {
            $services[] = array(
                'display_name' => $service_name,
                'service_object_id' => $service_id,
            );
        }

        return $services;
    }

    /**
     * @param $row
     * @return mixed
     */
    public function extractFirstHostId($row)
    {
        return $row['service_object_id'];
    }

    /**
     * returns array for the second level nodes
     * ================================================
     * This method is not used in CMDB, but needs to be
     * initialized since it is an abstract method from the
     * parent class
     *
     * @deprecated Unused
     */
    public function getSecondLevel($parent_id = null, $filter = null)
    {
    }

    /**
     * Helper function to get top level services
     *
     */
    public function getTopLevelServices()
    {
        $json_string_services = '
         {
            "32"    : "service 1",
            "12"    : "service 2",
            "123"   : "service 3",
            "453"   : "service 4",
            "3212"  : "service 5",
            "3765"  : "service 6"
         }
         ';

        return $json_string_services;
    }

    /**
     * @return bool
     */
    public function isAvailable()
    {
        return true;
    }
}