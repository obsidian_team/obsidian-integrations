<?php
namespace Obsidian\Rest;

use Obsidian\Rest\Exception\ObsidianApiException;
use Obsidian\Container;

class Dummy
{
    /**
     * @var Container
     */
    private $container;

    /**
     * Customer constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
    
    public function getTopLevelNodes()
    {}
    
    public function getSecondLevel($type_tool = null, $parent_id=0)
    {
        $type_tool = ($type_tool === 'cmdb') ? 'cmdb': 'integration' ;
        
    }
}